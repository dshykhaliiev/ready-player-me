import { Controller, Get, Param, Res } from '@nestjs/common';
import { InjectQueue } from '@nestjs/bull';
import { JobId, Queue } from 'bull';
import {
  GenerateResponseObject,
  Report,
  StatusObject,
} from './types/report.types';
import { ReportsService } from './services/reports.service';
import { ReportStatus } from './consts/status';
import { REPORTS_QUEUE } from './consts/general';
import { Response } from 'express';

@Controller('reports')
export class ReportsController {
  constructor(
    @InjectQueue(REPORTS_QUEUE) private reportsQueue: Queue,
    private readonly reportsService: ReportsService,
  ) {}

  @Get('generate/:currency')
  async generate(
    @Param('currency') currency: string,
  ): Promise<GenerateResponseObject> {
    const { id } = await this.reportsQueue.add(
      'generate',
      { currency },
      {
        removeOnComplete: true,
      },
    );

    const report: Report = {
      id,
      currency,
      originalCurrency: currency,
      status: ReportStatus.PREPARING,
    };
    this.reportsService.addReport(report);

    return { reportId: id, currency: currency };
  }

  @Get('status')
  status(): StatusObject[] {
    return this.reportsService.getAllStatuses();
  }

  @Get(':id')
  getReport(@Param('id') id: string, @Res() response: Response) {
    const { pdfData } = this.reportsService.getReportById(id);

    const stream = response.writeHead(200, {
      'Content-Type': 'application/pdf',
      'Content-disposition': `attachment;filename=report.pdf`,
    });

    pdfData.pipe(stream);
    pdfData.end();
  }
}
