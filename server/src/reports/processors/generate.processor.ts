import { Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';
import { ReportsService } from '../services/reports.service';
import { ReportStatus } from '../consts/status';
import { FetchDataService } from '../services/fetchData.service';
import { GenerateReportService } from '../services/generateReport.service';
import { Report } from '../types/report.types';

@Processor('reports-queue')
export class GenerateProcessor {
  constructor(
    private readonly reportsService: ReportsService,
    private readonly fetchDataService: FetchDataService,
    private readonly generateReportService: GenerateReportService,
  ) {}
  @Process('generate')
  async generateReport(job: Job): Promise<void> {
    const { id, data } = job;
    const { currency } = data;

    const ratesData = await this.fetchDataService.fetchByCurrency(currency);

    const pdfData = await this.generateReportService.generatePDF(id, ratesData);

    const report: Report = this.reportsService.getReportById(id);
    report.status = ReportStatus.READY;
    report.pdfData = pdfData;
    report.currency = ratesData.base;
  }
}
