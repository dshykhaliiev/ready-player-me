export enum ReportStatus {
  PREPARING = 'preparing',
  READY = 'ready',
}
