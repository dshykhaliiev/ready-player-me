import { Module } from '@nestjs/common';
import { ReportsController } from './reports.contoller';
import { ReportsService } from './services/reports.service';
import { BullModule } from '@nestjs/bull';
import { GenerateProcessor } from './processors/generate.processor';
import { FetchDataService } from './services/fetchData.service';
import { GenerateReportService } from './services/generateReport.service';
import { REPORTS_QUEUE } from './consts/general';

@Module({
  imports: [
    BullModule.forRoot({
      redis: {
        host: 'localhost',
        port: 6379,
      },
    }),
    BullModule.registerQueue({ name: REPORTS_QUEUE }),
  ],
  controllers: [ReportsController],
  providers: [
    GenerateProcessor,
    ReportsService,
    FetchDataService,
    GenerateReportService,
  ],
})
export class ReportsModule {}
