import { Injectable } from '@nestjs/common';
import { ITable } from 'pdfkit-table-ts';
import { PDFType, RateItemType } from '../types/pdf.types';
const { PDFDocument } = require('pdfkit-table-ts');

@Injectable()
export class GenerateReportService {
  public async generatePDF(id, ratesData): Promise<PDFType> {
    const {base: baseCurrency, date, rates} = ratesData;

    const doc = new PDFDocument();

    this.addHeader(doc, baseCurrency, date);

    this.addDataTable(doc, rates);

    return doc;
  }

  private addHeader(doc: PDFType, baseCurrency, date): void {
    doc
      .fontSize(25)
      .text('Exchange rates', {
        width: doc.width,
        align: 'center',
      });

    doc
      .fontSize(20)
      .text(`Base currency: ${baseCurrency}`, {
        width: doc.width,
        align: 'center',
      });

    doc
      .fontSize(18)
      .text(`${date}`, {
        width: doc.width,
        align: 'center',
      });
  }

  private addDataTable(doc: PDFType, rates: RateItemType[]): void {
    const table: ITable = {
      headers: ['Currency', 'Rate'],
      rows: Object.entries(rates) as string[][],
    };

    doc.table(table, {
      columnsSize: [ 80, 370 ],
    });
  }
}
