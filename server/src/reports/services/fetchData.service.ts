import { Injectable, Logger } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class FetchDataService {
  private readonly logger = new Logger(FetchDataService.name);
  public async fetchByCurrency(currency: string): Promise<Record<string, any>> {
    try {
      const response = await axios.request({
        baseURL: 'https://api.exchangerate.host/',
        url: 'latest/',
        method: 'GET',
        params: {
          base: currency,
        },
      });

      const { data } = response;

      if (data.base.toLowerCase() !== currency.toLowerCase()) {
        this.logger.warn(`No data found for ${currency}. Using EUR...`);
      }

      return data;
    } catch (error) {
      this.logger.error('Error fetching exchange rates: ', error);
    }
  }
}
