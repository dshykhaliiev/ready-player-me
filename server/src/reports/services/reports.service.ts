import { Injectable } from '@nestjs/common';
import { Report, StatusObject } from '../types/report.types';
import { JobId } from 'bull';

@Injectable()
export class ReportsService {
  private readonly reports: Report[];

  constructor() {
    this.reports = [];
  }

  public addReport(report: Report): void {
    this.reports.push(report);
  }

  public getReportById(id: JobId): Report {
    return this.reports.find((report: Report) => {
      return report.id === id;
    });
  }

  public getAllStatuses(): StatusObject[] {
    return this.reports.map(({ id, status, currency, originalCurrency }) => {
      return {
        id,
        status,
        currency,
        originalCurrency,
      };
    });
  }
}
