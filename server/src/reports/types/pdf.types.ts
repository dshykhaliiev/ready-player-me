import { IOptions, ITable } from 'pdfkit-table-ts';
import stream from 'stream';

export type RateItemType = {
  currency: string;
  rate: number;
};

export type TextOptsType = {
  align: string;
  width: number;
};

export type PDFType = {
  text: (text: string, ops?: TextOptsType) => PDFType;
  fontSize: (size: number) => PDFType;
  width: number;
  table: (table: ITable, opts?: IOptions) => PDFType;

  pipe: (stream: stream.Writable) => void;

  end: () => void;
};
