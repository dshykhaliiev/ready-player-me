import { JobId } from 'bull';
import { ReportStatus } from '../consts/status';
import { PDFType } from './pdf.types';

export type Report = {
  id: JobId;
  currency: string;

  originalCurrency: string;

  status: ReportStatus;

  pdfData?: PDFType;
};

export type StatusObject = {
  id: JobId;
  status: ReportStatus;
};

export type GenerateResponseObject = {
  reportId: JobId;
  currency: string;
};
