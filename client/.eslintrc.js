module.exports = {
  extends: [
    "react-app",
    "react-app/jest",
    "plugin:prettier/recommended",
    "eslint:recommended",
    "plugin:react/recommended"
  ],
  root: true,
  env: {
    es6: true,
    jest: true,
  },
  ignorePatterns: ['.eslintrc.js'],
  rules: {
    "singleQuote": 0,
    "react/react-in-jsx-scope": 0,
  },
};
