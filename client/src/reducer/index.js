import { combineReducers } from '@reduxjs/toolkit';
import reportsReducer from '../containers/ExchangeRates/slices/slice';
const rootReducer = combineReducers({
  reports: reportsReducer,
});

export { rootReducer };
