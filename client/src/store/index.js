import { configureStore } from '@reduxjs/toolkit';
import { rootReducer } from '../reducer';

const rootStore = configureStore({
  reducer: rootReducer,
});

export { rootStore };
