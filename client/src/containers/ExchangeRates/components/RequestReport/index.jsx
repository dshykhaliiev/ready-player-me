import { ButtonWrapper, ContentWrapper, PaperWrapper } from './styled';
import { Button, Paper, TextField } from '@mui/material';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { DEFAULT_CURRENCY } from '../../consts/consts';

const RequestReport = ({ onGenerateBtnClick }) => {
  const [currency, setCurrency] = useState(DEFAULT_CURRENCY);
  const onClick = (event) => {
    event.preventDefault();
    onGenerateBtnClick(currency);
  };

  const onChange = (event) => {
    setCurrency(event.target.value);
  };

  return (
    <PaperWrapper>
      <form>
        <Paper elevation={5}>
          <ContentWrapper>
            <TextField
              value={currency}
              onChange={onChange}
              label="currency"
              size="small"
              helperText="Please enter currency"
            />
            <ButtonWrapper>
              <Button
                variant="contained"
                size="medium"
                onClick={onClick}
                type="submit"
              >
                Generate
              </Button>
            </ButtonWrapper>
          </ContentWrapper>
        </Paper>
      </form>
    </PaperWrapper>
  );
};

RequestReport.propTypes = {
  onGenerateBtnClick: PropTypes.func,
};

export { RequestReport };
