import styled from '@emotion/styled';

export const PaperWrapper = styled.div({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

export const ContentWrapper = styled.div({
  display: 'flex',
  gap: '30px',
  alignItems: 'center',
  padding: '20px',
  width: '75vw',
});

export const ButtonWrapper = styled.div({
  marginTop: '-25px',
});
