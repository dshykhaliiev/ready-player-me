import { ItemWrapper, LabelWrapper } from './styled';
import { Alert, Button, FormLabel } from '@mui/material';
import { ReportStatus } from './consts/report-status';
import PropTypes from 'prop-types';

const ReportItem = ({ id, status, currency, originalCurrency, onDownloadBtnClick }) => {
  const color = status === ReportStatus.READY ? 'success' : 'warning';
  const label = status === ReportStatus.READY ? 'DOWNLOAD' : 'PREPARING';

  const onClick = () => {
    if (status !== ReportStatus.READY) {
      return;
    }

    onDownloadBtnClick(id, currency);
  };

  return (
    <ItemWrapper>
      <LabelWrapper>
        <FormLabel>{`report-${currency}.pdf`}</FormLabel>
      </LabelWrapper>
      <Button variant="contained" size="medium" color={color} onClick={onClick}>
        {label}
      </Button>
      {currency.toLowerCase() !== originalCurrency.toLowerCase() ? (
        <Alert variant="standard" color="warning">
          Base currency changed to EUR
        </Alert>
      ) : null}
    </ItemWrapper>
  );
};

export { ReportItem };

ReportItem.propTypes = {
  id: PropTypes.string,
  status: PropTypes.string,
  onDownloadBtnClick: PropTypes.func,
  currency: PropTypes.string,
  originalCurrency: PropTypes.string,
};
