import styled from '@emotion/styled';

export const LabelWrapper = styled.div({
  width: '200px',
});

export const ItemWrapper = styled.div({
  display: 'flex',
  gap: '50px',
  alignItems: 'center',
  width: '75vw',
});
