import PropTypes from 'prop-types';

export const ReportItemShape = PropTypes.shape({
  id: PropTypes.string,
  status: PropTypes.string,
});
