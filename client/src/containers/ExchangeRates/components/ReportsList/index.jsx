import { Paper } from '@mui/material';
import { ContentWrapper, PaperWrapper } from './styled';
import { ReportItem } from '../ReportItem';
import PropTypes from 'prop-types';
import { ReportItemShape } from '../ReportItem/shapes/shapes';

const ReportsList = ({ reports, onDownloadBtnClick }) => {
  return (
    <PaperWrapper>
      <Paper elevation={5}>
        <ContentWrapper>
          {reports.map((report) => (
            <ReportItem
              key={report.id}
              id={report.id}
              status={report.status}
              currency={report.currency}
              originalCurrency={report.originalCurrency}
              onDownloadBtnClick={onDownloadBtnClick}
            />
          ))}
        </ContentWrapper>
      </Paper>
    </PaperWrapper>
  );
};

export { ReportsList };

ReportsList.propTypes = {
  reports: PropTypes.arrayOf(ReportItemShape),
  onDownloadBtnClick: PropTypes.func,
};
