import styled from '@emotion/styled';

export const PaperWrapper = styled.div({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

export const ContentWrapper = styled.div({
  display: 'flex',
  flexDirection: 'column',
  gap: '30px',
  alignItems: 'center',
  padding: '20px',
  width: '75vw',
});
