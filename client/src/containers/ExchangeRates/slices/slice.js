import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { sendGenerateReportRequest, sendReportDownloadRequest, sendStatusRequest } from '../api/api';
import { ReportStatus } from '../components/ReportItem/consts/report-status';

const initialState = {
  reports: [],
};

export const requestGenerateReport = createAsyncThunk('reports/requestGenerate', async (currency) => {
  const { data } = await sendGenerateReportRequest(currency);
  return data;
});

export const requestReportsStatus = createAsyncThunk('reports/status', async () => {
  const { data } = await sendStatusRequest();
  return data;
});

export const requestDownloadReport = createAsyncThunk('reports/requestDownload', async ({ id, currency }) => {
  const { data } = await sendReportDownloadRequest(id);

  const fileURL = window.URL.createObjectURL(data);
  let alink = document.createElement('a');
  alink.href = fileURL;
  alink.download = `report-${currency}.pdf`;
  alink.click();

  return id;
});

const slice = createSlice({
  name: 'reports',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(requestGenerateReport.fulfilled, (state, action) => {
      state.reports.push({
        id: action.payload.reportId,
        status: ReportStatus.PREPARING,
        currency: action.payload.currency,
        originalCurrency: action.payload.currency,
      });
    });

    builder.addCase(requestReportsStatus.fulfilled, (state, action) => {
      const { reports } = state;
      const statusMap = action.payload.reduce((map, reportStatus) => {
        const { id, status, currency, originalCurrency } = reportStatus;
        map[id] = { status, currency, originalCurrency };

        return map;
      }, {});

      reports.forEach((report) => {
        const { status, currency, originalCurrency } = statusMap[report.id];

        report.status = status;
        report.currency = currency;
        report.originalCurrency = originalCurrency;
      });
    });
  },
});

export const selectReports = (state) => state.reports.reports;
export default slice.reducer;
