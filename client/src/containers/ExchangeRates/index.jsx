import { ExchangeRatesWrapper } from './styled';
import { RequestReport } from './components/RequestReport';
import { ReportsList } from './components/ReportsList';
import { useDispatch, useSelector } from 'react-redux';
import { useCallback, useEffect } from 'react';
import { requestDownloadReport, requestGenerateReport, requestReportsStatus, selectReports } from './slices/slice';
import { usePolling } from './hooks/usePolling';
import { REPORTS_STATUS_POLLING_INTERVAL } from './consts/consts';

const ExchangeRates = () => {
  const dispatch = useDispatch();
  const reports = useSelector(selectReports);

  const { startPolling, stopPolling } = usePolling(requestReportsStatus, REPORTS_STATUS_POLLING_INTERVAL);

  useEffect(() => {
    startPolling();

    return () => {
      stopPolling();
    };
  }, []);

  const onGenerateBtnClicked = useCallback(
    (currency) => {
      dispatch(requestGenerateReport(currency));
    },
    [dispatch],
  );

  const onDownloadBtnClicked = useCallback(
    (id, currency) => {
      dispatch(requestDownloadReport({ id, currency }));
    },
    [dispatch],
  );

  return (
    <ExchangeRatesWrapper>
      <RequestReport onGenerateBtnClick={onGenerateBtnClicked} />
      {reports.length ? <ReportsList reports={reports} onDownloadBtnClick={onDownloadBtnClicked} /> : null}
    </ExchangeRatesWrapper>
  );
};

export { ExchangeRates };
