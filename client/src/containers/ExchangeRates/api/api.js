import axios from 'axios';

async function sendGenerateReportRequest(currency) {
  try {
    const response = await axios.request({
      baseURL: 'http://localhost:3001/',
      url: `reports/generate/${currency}`,
      method: 'GET',
    });

    const { data } = response;
    return { data };
  } catch (error) {
    console.log('Error requesting to generate report: ', error);
  }
}

async function sendStatusRequest() {
  try {
    const response = await axios.request({
      baseURL: 'http://localhost:3001/',
      url: 'reports/status',
      method: 'GET',
    });

    const { data } = response;
    return { data };
  } catch (error) {
    console.log('Error requesting status: ', error);
  }
}

async function sendReportDownloadRequest(id) {
  try {
    const response = await axios.request({
      baseURL: 'http://localhost:3001/',
      url: `reports/${id}`,
      responseType: 'blob',
      method: 'GET',
      timeout: 60000,
    });

    const { data } = response;
    return { data };
  } catch (error) {
    console.log(`Error downloading report ${id}: `, error);
  }
}

export { sendGenerateReportRequest, sendStatusRequest, sendReportDownloadRequest };
