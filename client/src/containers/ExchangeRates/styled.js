import styled from '@emotion/styled';

export const ExchangeRatesWrapper = styled.div({
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'column',
  alignItems: 'center',
  width: '100%',
  paddingTop: '30px',
  gap: '20px',
});
