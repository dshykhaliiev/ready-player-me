import { useDispatch } from 'react-redux';
import { useRef } from 'react';

export const usePolling = (thunk, interval = 0) => {
  const dispatch = useDispatch();
  const intervalIdRef = useRef(0);

  const startPolling = () => {
    if (intervalIdRef.current) {
      clearInterval(intervalIdRef.current);
    }

    const id = setInterval(() => {
      dispatch(thunk());
    }, interval);

    intervalIdRef.current = id;
  };
  const stopPolling = () => {
    clearInterval(intervalIdRef.current);
  };

  return { stopPolling, startPolling };
};
